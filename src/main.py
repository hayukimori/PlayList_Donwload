#!/usr/bin/env python3
import pytube
import subprocess, os, sys
from rich.console import Console

console = Console()

HOME = os.getenv("HOME")

class Downloader:
	def d_audiopl(url) -> None:
		subprocess.call("clear", shell=True)

		p = pytube.Playlist(url)
		console.print(f"[bold][blue]::[/] {p.title} [/]")

		playlist_elements = p.length
		counter = 0

		pltitle = f"{HOME}/Música"

		p2 = p.owner + "/" + p.title
		pltitle_2 = str(p2).lower()

		ptitle = pltitle = pltitle + "/" + pltitle_2

		with console.status(f'[blue] Baixando... [/]'):
			for audio in p.videos:
				console.print(f"[bold][purple] {str(counter)} ->[/] {audio.title} [/]")
				counter += 1

				try:
					audio.streams.get_by_itag(140).download(pltitle)
				except:
					audio.streams.get_audio_only().download(pltitle)

		if playlist_elements != counter:
			console.print(f"[bold][yellow] | [/][/] Nem todos os arquivos foram baixados.\n Número da perda: {playlist_elements - counter}")
		elif(playlist_elements - counter) < 0:
			console.print(f"[bold][green] >> [/][/] Todos os audios foram baixados com sucesso: {playlist_elements} audios baixados.")
		else:
			console.print(f"[bold][green] >> [/][/] Todos os audios foram baixados com sucesso: {playlist_elements} audios baixados.")
		

	def d_videopl(url) -> None:
		subprocess.call("clear", shell=True)

		p = pytube.Playlist(url)
		console.print(f"[bold][blue]::[/] {p.title} [/]")

		playlist_elements = p.length
		counter = 0

		pltitle = f"{HOME}/Vídeos"

		p2 = p.owner + "/" + p.title
		pltitle_2 = str(p2).lower()
		pltitle = pltitle + "/" + pltitle_2

		with console.status(f'[blue] Baixando... [/]'):
			for video in p.videos:
				console.print(f"[bold][purple] {str(counter)} ->[/] {video.title} [/]")
				counter += 1

				try:
					video.streams.get_by_itag(22).download(pltitle)
				except:
					video.streams.get_highest_resolution().download(pltitle)

		if playlist_elements != counter:
			console.print(f"[bold][yellow] | [/][/] Nem todos os vídoes foram baixados.\n Número da perda: {playlist_elements - counter}")

		elif((playlist_elements -counter) < 0):
			console.print(f"[bold][green] >> [/][/] Todos os audios foram baixados com sucesso: {playlist_elements} audios baixados")

		else:
			console.print(f"[bold][green] >>[/] Todos os audios foram baixados com sucesso: {playlist_elements} audios baixados.[/]")

		pass


if __name__ == "__main__":
	download_type = console.input("[bold][blue]::[/] Tipo de playlist que deseja baixar (audio / video): [/]")
	
	if len(download_type) < 3:
		console.print("[bold][red]!![/][/] Tipo de download não especificado. (apenas \"audio\" ou \"video\" são aceitos.")
		sys.exit(1)
	else:
		url = console.input("[bold][blue]::[/] Digite ou cole a url da playlist: [/]")

		if download_type == "audio":
			Downloader.d_audiopl(url)
		if download_type == "video":
			Downloader.d_videopl(url)