# PlayList_Donwload

Um script para baixar playlists do Youtube. 

## Instalação

Para instalar essa aplicação utilizamos o comando make

```bash

make

```

O make está configurado para instalar os requisitos da aplicação com:<br>

`pip install -r requirements.txt`

Em seguida damos permissão de execução com: <br>

`chmod +x src/main.py`

E finalizamos movendo o script para o path correto:<br>

`sudo cp src/main.py /usr/bin/pydownload`

## Features:

1. Baixar playlists no formato de aúdio
2. Baixar playlists no formato de vídeo 
3. Colocar as playlists de forma organizada nas respectivas pastas `$HOME/Música/` e `$HOME/Vídeos/`

### Futuras Atualizações:
  - [] adicionar Barra de progresso aos downloads
  - [] conectar ao adb shell para salvar downloads no celular
